chrome.browserAction.setTitle({title:chrome.i18n.getMessage("title")});

var api = '', user = '';

document.getElementById("btn_submit").value = chrome.i18n.getMessage("saveAPI");

document.getElementById("descr").innerHTML = chrome.i18n.getMessage("optionDescr") +
' <a href="https://kano.is/index.php?k=userset" target="_blank" title ="https://kano.is/index.php?k=userset">Kano.is</a>.';

chrome.storage.sync.get(function (obj) {
  if (obj.apikey!=null) {
    api = obj.apikey;
    document.getElementById("input").value = api;
  } else {
    document.getElementById("input").value = chrome.i18n.getMessage("setAPI");
  }
  if (obj.apiuser!=null) {
    user = obj.apiuser;
    document.getElementById("inputUser").value = user;
  } else {
    document.getElementById("inputUser").value = chrome.i18n.getMessage("setUser");
  }
});

function clickHandler(e) {
  var input = document.getElementById("input").value;
  var inputuser = document.getElementById("inputUser").value;
  chrome.storage.sync.set({"apikey": input});
  chrome.storage.sync.set({"apiuser": inputuser});
  window.close();
  
}

document.addEventListener( "DOMContentLoaded" , function () {
  document.getElementById("btn_submit").addEventListener( "click" , clickHandler);
});