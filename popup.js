chrome.browserAction.setTitle({title:chrome.i18n.getMessage("title")});

var api = '', user = '';

chrome.storage.sync.get(function (obj) {
  if (obj.apikey!=null) {
    api = obj.apikey;
    document.getElementById("input").textContent = api;
  } else {
    document.getElementById("input").textContent = chrome.i18n.getMessage("setAPI");
  }


  if (obj.apiuser!=null) {
    user = obj.apiuser;
    document.getElementById("inputUser").textContent = user;
  } else {
    document.getElementById("inputUser").textContent = chrome.i18n.getMessage("setUser");
  }
  getData();
});

function getData() {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "https://kano.is/index.php?k=api&username=" + user + "&api=" + api + "&json=y&work=y", true);
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
      
      var json = xhr.responseText;
      
      if (json.indexOf('nil') > -1) {
        console.log('Error API');
        var html = '<p><h1>API error.</h1><a href="options.html" title ="Extension Options">' +
                    chrome.i18n.getMessage("setAPIandUser") + '</a>';
      } else {
        
        json = json.replace(/^[^(]*\(([\S\s]+)\);?$/, '$1'); // Turn JSONP in JSON
        json = JSON.parse(json);
      
        var rows = json['rows'];
        
        var name = '', rate5min = 0, rate1h = 0, diff = 0, last =0, rate5minTotal = 0, rate1hTotal = 0;
        var html = '<p>';
        var now = new Date();
        
        for (var i = 0; i < rows; i++) {
          name = json['workername:' + i];
          rate5min = Math.round(json['w_hashrate5m:' + i] / 1000000000);
          rate1h = Math.round(json['w_hashrate1hr:' + i] / 1000000000);
          diff = Math.round(json['w_lastdiff:' + i]);
          last = Math.round(now.getTime() / 1000 - json['w_lastshare:' + i]);
          
          html += '<div><div style="text-align:center;"><b>' + name + '</b></div>' +
                  '<div style="float:left;">5min: <b>' + rate5min + ' GHs</b></br>1hour: <b>' + rate1h + ' GHs</b></br></div>' +
                  '<div style="float:right;">diff: ' + diff + ' k</br>' +
                  'last: ' + last + ' s</div></div><p></br>';
          
          rate5minTotal += rate5min;
          rate1hTotal += rate1h;
        }
    
        html += '<hr><div style="text-align:center;"><b>' + chrome.i18n.getMessage("total") + '</b></div>' +
                '5min: <b>' + rate5minTotal + ' GHs</b></br>1hour: <b>' + rate1hTotal + ' GHs</b></br>';
      }
      
      var dannie = document.getElementById('dannie');
      dannie.style.width = "240px";
      dannie.innerHTML = html;
  
    }
  }
  xhr.send();
}

var xhrStat = new XMLHttpRequest();
xhrStat.open("GET", "https://kano.is/index.php?k=blocks", true);
xhrStat.onreadystatechange = function() {
  if (xhrStat.readyState == 4) {
    var table = xhrStat.responseText.match( /<td class=dr>(\d{1,3}.\d{1,2}%)<\/td><\/tr>/ig );
    //console.log(table); 
    
    var html = '<hr><div style="text-align:center;"><b>' + chrome.i18n.getMessage("luck") + '</b></div>' +
              'Last 5 Blocks: <b>' + table[0] + '</b></br>' +
              'Last 10 Blocks: <b>' + table[1] + '</b></br>' +
              'Last 25 Blocks: <b>' + table[2] + '</b></br>' +
              'Last 50 Blocks: <b>' + table[3] + '</b></br>' +
              'Last 100 Blocks: <b>' + table[4] + '</b></br>' +
              'Last 250 Blocks: <b>' + table[5] + '</b></br>' +
              'Last 500 Blocks: <b>' + table[6] + '</b></br>' +
              'All: <b>' + table[7] + '</b>' +
              '<div style="text-align:right;"><a href="http://bcmob.com.ua/" title ="http://bcmob.com.ua/"><b>BCmob</b></a> Ver: 1.9</br></div>';
    

    var stat = document.getElementById('stat');
    stat.style.width="240px";
    stat.innerHTML = html;

  }
}
xhrStat.send();